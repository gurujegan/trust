package com.manyuyantra.accesingdatamysql;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.manyuyantra.accesingdatamysql.service.UserService;

@CrossOrigin(origins = "*")
@RestController // This means that this class is a Controller
@RequestMapping(path="/demo") // This means URL's start with /demo (after Application path)
public class MainController {
  @Autowired // This means to get the bean called userRepository
         // Which is auto-generated by Spring, we will use it to handle the data
  private UserService userService;

//  @PostMapping(path="/add") // Map ONLY POST Requests
//  public @ResponseBody String addNewUser (@RequestParam String name
//      , @RequestParam String email) {
//    // @ResponseBody means the returned String is the response, not a view name
//    // @RequestParam means it is a parameter from the GET or POST request
//
//    User n = new User();
//    n.setUsername(name);
//    n.setEmail(email);
//    userRepository.save(n);
//    return "Saved";
//  }
//
//  @GetMapping(path="/all")
//  public @ResponseBody Iterable<User> getAllUsers() {
//    // This returns a JSON or XML with the users
//    return userRepository.findAll();
//  }
  //Get All users
  @GetMapping("/users")
  public @ResponseBody List<User> all() {
    return userService.getAllUsers();
  }
  
  @PostMapping("/users")
  User newUser(@RequestBody User user)
  {
	  return userService.addUser(user);
  }
  
  // Single item

  @GetMapping("/users/{id}")
  User one(@PathVariable int id) {
	  	return userService.getSingleUser(id);
  }
  
  @PutMapping("/users/{id}")
  User replaceEmployee(@RequestBody User newUser, @PathVariable int id) {

return userService.modifyUser(newUser, id);
  }
  
  @DeleteMapping("/users/{id}")
  void deleteEmployee(@PathVariable int id) {
    userService.deleteUser(id);
  }
  
}