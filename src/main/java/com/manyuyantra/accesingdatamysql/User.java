package com.manyuyantra.accesingdatamysql;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class User implements Serializable {
	
@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", role_id_fk=" + role_id_fk + "]";
	}

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Integer id;

private String username;
private String password;
private String email;
private int role_id_fk;

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public int getRole_id_fk() {
	return role_id_fk;
}

public void setRole_id_fk(int role_id_fk) {
	this.role_id_fk = role_id_fk;
}


User(){}

User(String name, String pass, String email)
{
	this.username = name;
	this.password = pass;
	this.email = email;
	
}



}
