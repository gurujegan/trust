package com.manyuyantra.accesingdatamysql.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manyuyantra.accesingdatamysql.User;
import com.manyuyantra.accesingdatamysql.UserNotFoundException;
import com.manyuyantra.accesingdatamysql.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;

	@Override
	public User addUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public User modifyUser(User newUser, int id) {
		   return userRepository.findById(id)
				      .map(user -> {
				        user.setUsername(newUser.getUsername());
				        user.setPassword(newUser.getPassword());
				        user.setEmail(newUser.getEmail());
				        user.setRole_id_fk(newUser.getRole_id_fk());
				        return userRepository.save(user);
				      })
				      .orElseGet(() -> {
				    	  newUser.setId(id);
				        return userRepository.save(newUser);
				      });
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		 userRepository.deleteById(id);
	}
	
	@Override
	public User getSingleUser(int id) {
		// TODO Auto-generated method stub
		
		return userRepository.findById(id)
			      .orElseThrow(() -> new UserNotFoundException(id));
	}

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		List<User> users = userRepository.findAll();
		return users;

	}

}
