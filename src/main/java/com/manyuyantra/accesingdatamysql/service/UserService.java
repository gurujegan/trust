package com.manyuyantra.accesingdatamysql.service;

import java.util.List;

import com.manyuyantra.accesingdatamysql.User;

public interface UserService {
	
	public User addUser(User user);
	
	public void deleteUser(int id);
	public List<User> getAllUsers();
    public User getSingleUser(int id);
	public User modifyUser(User newUser, int id);

}
